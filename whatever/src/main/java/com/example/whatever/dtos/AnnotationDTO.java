package com.example.whatever.dtos;


public class AnnotationDTO {

    private Integer id;
    private Integer patientId;
    private Integer activityId;
    private String annotation;
    private String recommendation;

    public AnnotationDTO() {
    }

    public AnnotationDTO(Integer id, Integer patientId, Integer activityId, String annotation, String recommendation){
        this.id = id;
        this.patientId = patientId;
        this.activityId = activityId;
        this.annotation = annotation;
        this.recommendation = recommendation;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }









    public static class Builder {
        private Integer nestedid;
        private Integer nestedpatientid;
        private Integer nestedactivityid;
        private String nestedannotation;
        private String nestedrecommendation;

        public Builder id(int id) {
            this.nestedid = id;
            return this;
        }

        public Builder patient_id(Integer id) {
            this.nestedpatientid = id;
            return this;
        }

        public Builder activity(Integer nestedactivity) {
            this.nestedactivityid = nestedactivity;
            return this;
        }

        public Builder starttime(String nestedannotation) {
            this.nestedannotation = nestedannotation;
            return this;
        }

        public Builder endtime(String nestedrecommendation) {
            this.nestedrecommendation = nestedrecommendation;
            return this;
        }

        public AnnotationDTO create() {
            return new AnnotationDTO(nestedid, nestedpatientid, nestedactivityid, nestedannotation, nestedrecommendation);
        }

    }

}