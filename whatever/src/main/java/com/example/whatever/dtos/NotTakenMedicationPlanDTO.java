package com.example.whatever.dtos;


public class NotTakenMedicationPlanDTO implements java.io.Serializable{

    private Integer id;
    private Integer patient;
    private String medication;
    private Integer interval;
    private Integer period;

    public NotTakenMedicationPlanDTO() {
    }

    public NotTakenMedicationPlanDTO(Integer id, Integer patient, String medication, Integer interval, Integer period) {
        super();
        this.id = id;
        this.patient = patient;
        this.medication = medication;
        this.interval = interval;
        this.period = period;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPatient() {
        return patient;
    }

    public void setPatient(Integer patient) {
        this.patient = patient;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }




    public static class Builder {
        private Integer nestedid;
        private Integer nestedpatient;
        private String nestedmedication;
        private Integer nestedinterval;
        private Integer nestedperiod;

        public Builder id(int id) {
            this.nestedid = id;
            return this;
        }

        public Builder patient(Integer patient) {
            this.nestedpatient = patient;
            return this;
        }

        public Builder medication(String medication) {
            this.nestedmedication = medication;
            return this;
        }

        public Builder interval(Integer interval) {
            this.nestedinterval = interval;
            return this;
        }

        public Builder period(Integer period) {
            this.nestedperiod= period;
            return this;
        }

        public NotTakenMedicationPlanDTO create() {
            return new NotTakenMedicationPlanDTO(nestedid, nestedpatient, nestedmedication, nestedinterval, nestedperiod);
        }

    }

}

