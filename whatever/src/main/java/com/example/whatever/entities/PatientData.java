package com.example.whatever.entities;


import javax.persistence.*;

@Entity
@Table(name = "infos")
public class PatientData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String patient_id;
    private String activity;
    private Long startTime;
    private Long endTime;

    public PatientData(){

    }

    public PatientData(String patient_id, String activity, Long startTime, Long endTime) {
        this.patient_id = patient_id;
        this.activity = activity;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public boolean isActivityNormal() {
        final long HOUR_TO_MILLIS = 3600 * 1000;
        final long MAX_SLEEP_HOURS = 12 * HOUR_TO_MILLIS;
        final long MAX_LEAVE_HOURS = 12 * HOUR_TO_MILLIS;
        final long MAX_TOILET_HOURS = 1 * HOUR_TO_MILLIS;
        final String SLEEPING_ACTIVITY = "Sleeping";
        final String LEAVING_ACTIVITY = "Leaving";
        final String TOILETING_ACTIVITY = "Toileting";

        long activityDuration = endTime - startTime;

        if ((activity.contains(SLEEPING_ACTIVITY) && activityDuration > MAX_SLEEP_HOURS)
                || activity.contains(LEAVING_ACTIVITY) && activityDuration > MAX_LEAVE_HOURS
                || activity.contains(TOILETING_ACTIVITY) && activityDuration > MAX_TOILET_HOURS){
            return false;
        }
        else{
            return true;
        }
    }

    @Override
    public String toString()
    {
        return "\n{\n patient_id : " + this.getPatient_id() +
                "\n activity : " + this.getActivity() +
                "\n start : " + this.getStartTime().toString() +
                "\n end : " + this.getEndTime().toString() +
                "\n}\n";
    }
}