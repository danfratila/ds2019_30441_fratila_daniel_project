package com.example.whatever.controllers;

import java.util.List;

import com.example.whatever.dtos.DoctorDTO;
import com.example.whatever.services.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/doctor")
public class DoctorController {

	@Autowired
	private DoctorService doctorService;

	// create
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertDoctor(@RequestBody DoctorDTO doctorDTO) {
		return doctorService.create(doctorDTO);
	}
	
	// read
	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public DoctorDTO getDoctorById(@PathVariable("id") int id) {
		return doctorService.findDoctorById(id);
	}

	// read
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<DoctorDTO> getAllDoctors() {
		return doctorService.findAll();
	}

	// update
	//@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	
	// delete
	//@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
}

