package com.example.whatever.controllers;

import com.example.whatever.dtos.UserDTO;
import com.example.whatever.entities.PatientData;
import com.example.whatever.services.ReceiverService;
import com.example.whatever.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;


    @RequestMapping(method = RequestMethod.GET)
    public UserDTO printTest() {
        System.out.println("test");
        return new UserDTO();
    }

    // read
    @RequestMapping(value = "/{username}/{password}", method = RequestMethod.GET)
    public UserDTO getSpecificUser(@PathVariable("username") String username, @PathVariable("password") String password) throws Exception {
        return userService.getSpecifiedUser(username, password);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<UserDTO> getAllUsers() {
        return userService.findAll();
    }
}


