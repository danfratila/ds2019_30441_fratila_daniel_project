package com.example.whatever.soap;

import com.example.whatever.dtos.AnnotationDTO;
import com.example.whatever.dtos.PatientDataDTO;
import com.example.whatever.entities.Annotation;
import com.example.whatever.entities.PatientData;
import com.example.whatever.repos.AnnotationRepo;
import com.example.whatever.repos.PatientDataRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
@WebService(endpointInterface = "com.example.whatever.soap.ModelService")
public class ModelServiceImpl implements ModelService{

    @Autowired
    private PatientDataRepo patientDataRepo;

    @Autowired
    private AnnotationRepo annotationRepo;

    @Override
    public List<PatientDataDTO> viewActivityHistory(int patientId) {
        System.out.println("CALL LA VIEW ACTIVITY HISTORY " + patientId);
        List<PatientDataDTO> history = new ArrayList<>();
        List<PatientData> patientDataList = patientDataRepo.findAll();
        for(PatientData iterator: patientDataList){
            if(Integer.parseInt(iterator.getPatient_id()) == patientId){
                history.add(new PatientDataDTO(iterator.getId(),
                        iterator.getPatient_id(),
                        iterator.getActivity(),
                        new Date(iterator.getStartTime()).toString(),
                        new Date(iterator.getEndTime()).toString()));
            }
        }
        return history;
    }

    @Override
    public List<String> viewActivityNames() {
        List<String> listOfActivityNames = new ArrayList<>();
        List<PatientData> patientDataList = patientDataRepo.findAll();
        for(PatientData iterator: patientDataList){
            if(!listOfActivityNames.contains(iterator.getActivity())){
                listOfActivityNames.add(iterator.getActivity());
            }
        }
        return listOfActivityNames ;
    }

    @Override
    public Long viewDataForActivity(String activityName) {
        long totalDuration = 0;
        List<PatientData> patientDataList = patientDataRepo.findAll();
        for(PatientData iterator: patientDataList){
            if(iterator.getActivity().equals(activityName)){
                totalDuration += (iterator.getEndTime() - iterator.getStartTime());
            }
        }
        return totalDuration;
    }

    @Override
    public void viewTakenPlanAtDay(int patientId, String date) {
        System.out.println("CALL LA VIEW TAKEN (nu e void aici) " + patientId + "; " + date);
    }

    @Override
    public List<PatientDataDTO> viewAbnormalActivities(int patientId) {
        System.out.println("CALL LA VIEW ABNORMAL ACTIVITIES");
        List<PatientDataDTO> abnormalActivities = new ArrayList<>();
        List<PatientData> patientDataList = patientDataRepo.findAll();
        for(PatientData iterator: patientDataList){
            if(!iterator.isActivityNormal()){
                abnormalActivities.add(new PatientDataDTO(iterator.getId(),
                        iterator.getPatient_id(),
                        iterator.getActivity(),
                        iterator.getStartTime().toString(),
                        iterator.getEndTime().toString())
                );
            }
        }
        return abnormalActivities;
    }

    @Override
    public void annotatePatientBehavior(AnnotationDTO annotationDTO) {
        System.out.println("CALL LA ANNOTATE "
                + annotationDTO.getActivityId()  + "-----"
                + annotationDTO.getAnnotation());
        List<Annotation> listOfAnnotations = annotationRepo.findAll();
        boolean exists = false;
        for(Annotation iterator: listOfAnnotations){
            if(iterator.getActivityId().equals(annotationDTO.getActivityId())){
                iterator.setIsNormal(annotationDTO.getAnnotation());
                annotationRepo.save(iterator);
                exists = true;
            }
        }
        if(!exists){
            Annotation annotation = new Annotation(1, annotationDTO.getActivityId(), annotationDTO.getAnnotation(),
                    annotationDTO.getRecommendation());
            annotationRepo.save(annotation);
        }
    }

    @Override
    public void addRecommendation(AnnotationDTO annotationDTO) {
        System.out.println("CALL LA RECOMMEND "
                + annotationDTO.getActivityId()  + "-----"
                + annotationDTO.getRecommendation());
        List<Annotation> listOfAnnotations = annotationRepo.findAll();
        boolean exists = false;
        for(Annotation iterator: listOfAnnotations){
            if(iterator.getActivityId().equals(annotationDTO.getActivityId())){
                iterator.setRecommendation(annotationDTO.getRecommendation());
                annotationRepo.save(iterator);                exists = true;
            }
        }
        if(!exists){
            Annotation annotation = new Annotation(1, annotationDTO.getActivityId(), annotationDTO.getAnnotation(),
                    annotationDTO.getRecommendation());
            annotationRepo.save(annotation);
        }
    }
}
