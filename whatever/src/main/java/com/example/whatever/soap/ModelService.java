package com.example.whatever.soap;

import com.example.whatever.dtos.AnnotationDTO;
import com.example.whatever.dtos.PatientDataDTO;
import com.example.whatever.entities.PatientData;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@WebService
public interface ModelService {

    // test purpose only
//    @WebMethod
//    TestModel getTestModel(int id);

    @WebMethod
    List<PatientDataDTO> viewActivityHistory(int patientId);

    @WebMethod
    List<String> viewActivityNames();

    @WebMethod
    Long viewDataForActivity(String activityName);

    @WebMethod
    void viewTakenPlanAtDay(int patientId, String date);

    @WebMethod
    List<PatientDataDTO> viewAbnormalActivities(int patientId);

    @WebMethod
    void annotatePatientBehavior(AnnotationDTO annotationDTO);

    @WebMethod
    void addRecommendation(AnnotationDTO annotationDTO);

}
