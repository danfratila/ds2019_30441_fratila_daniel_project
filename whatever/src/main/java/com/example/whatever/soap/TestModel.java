package com.example.whatever.soap;

public class TestModel {
    private int id;
    private String firstName;

    public TestModel(){
        id = 15;
        firstName = "da";
    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getFirstName(){
        return firstName;
    }

    public void setFirstName(String firstName){
        this.firstName = firstName;
    }

}
