package com.example.whatever.services;

import com.example.whatever.dtos.DoctorDTO;
import com.example.whatever.entities.Doctor;
import com.example.whatever.entities.User;
import com.example.whatever.repos.DoctorRepo;
import com.example.whatever.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.whatever.services.UtilityServices.extractNames;


@Service
public class DoctorService {


	@Autowired
	private DoctorRepo doctorRepository;

	@Autowired
	private UserRepo userRepository;

	public DoctorDTO findDoctorById(int id) {
		Optional<User> usr = userRepository.findById(id);
		Optional<Doctor> patient = doctorRepository.findById(id);
		DoctorDTO dto = new DoctorDTO();
		if (!usr.isPresent() || !patient.isPresent()) {
			//throw new ResourceNotFoundException(Doctor.class.getSimpleName());
		}
		else{
			String[] names = extractNames(usr.get().getName());

			dto = new DoctorDTO.Builder()
					.id(usr.get().getId())
					.firstname(names[0])
					.surname(names[1])
					.birthdate(usr.get().getBirthdate())
					.gender(usr.get().getGender())
					.address(usr.get().getAddress())
					.create();
		}
		return dto;
	}

	public List<DoctorDTO> findAll() {
		List<Doctor> doctors = doctorRepository.findAll();
		List<DoctorDTO> toReturn = new ArrayList<>();
		for (Doctor doctor: doctors) {
			Optional<User> user = userRepository.findById(doctor.getId());
			String[] names = extractNames(user.get().getName());
			DoctorDTO dto = new DoctorDTO.Builder()
					.id(user.get().getId())
					.firstname(names[0])
					.surname(names[1])
					.birthdate(user.get().getBirthdate())
					.gender(user.get().getGender())
					.address(user.get().getAddress())
					.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int create(DoctorDTO doctorDTO) {
		// List<String> validationErrors = validateUser(userDTO);
//		if (!validationErrors.isEmpty()) {
//			throw new EntityValidationException(User.class.getSimpleName(),validationErrors);
//		}

		Doctor doctor = new Doctor();

		Doctor doc = doctorRepository.save(doctor);
		return doc.getId();
	}
}
