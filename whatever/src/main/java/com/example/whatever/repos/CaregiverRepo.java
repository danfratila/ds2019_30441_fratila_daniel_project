package com.example.whatever.repos;

import java.util.Optional;
import com.example.whatever.entities.Caregiver;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CaregiverRepo extends JpaRepository<Caregiver, Integer> {

    Optional<Caregiver> findById(int id);

}