package com.example.whatever.repos;

import com.example.whatever.entities.PatientData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientDataRepo extends JpaRepository<PatientData, Integer> {


}