package com.example.whatever.repos;

import com.example.whatever.entities.Annotation;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AnnotationRepo extends JpaRepository<Annotation, Integer> {

}