package com.example.whatever.repos;

import com.example.whatever.entities.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;


public interface MedicationPlanRepo extends JpaRepository<MedicationPlan, Integer> {


}