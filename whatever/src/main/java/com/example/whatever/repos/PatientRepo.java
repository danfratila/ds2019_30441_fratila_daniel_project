package com.example.whatever.repos;

import java.util.Optional;
import com.example.whatever.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PatientRepo extends JpaRepository<Patient, Integer> {

    Optional<Patient> findById(int id);

}