package com.example.whatever.repos;

import com.example.whatever.entities.TakenMedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TakenMedicationPlanRepo extends JpaRepository<TakenMedicationPlan, Integer> {


}