package com.example.whatever.repos;

import com.example.whatever.entities.NotTakenMedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;


public interface NotTakenMedicationPlanRepo extends JpaRepository<NotTakenMedicationPlan, Integer> {


}