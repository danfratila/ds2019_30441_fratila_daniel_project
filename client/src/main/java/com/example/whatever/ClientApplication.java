package com.example.whatever;



import com.example.whatever.controllers.Controller;
import com.example.whatever.rmi.RMIServiceInterface;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;


public class ClientApplication {

	@Bean
	RmiProxyFactoryBean rmiProxy() {
		RmiProxyFactoryBean bean = new RmiProxyFactoryBean();
		bean.setServiceInterface(RMIServiceInterface.class);
		bean.setServiceUrl("rmi://localhost:1099/helloworldrmi");

		return bean;
	}

	public static void main(String[] args)
	{
		System.setProperty("java.awt.headless", "false");

		RMIServiceInterface helloWorldRMI =
				SpringApplication.run(ClientApplication.class, args).getBean(RMIServiceInterface.class);


		Controller ctrl = new Controller(helloWorldRMI);
		ctrl.run();
	}
}
