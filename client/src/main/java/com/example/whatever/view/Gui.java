package com.example.whatever.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class Gui {

    public JFrame f;
    public JPanel p;
    public JPanel infoPanel;
    public JLabel timestampLabel;
    public JPanel container;

    public Gui(){
        f = new JFrame("label");
        container = new JPanel();
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        f.add(container);

        timestampLabel = new JLabel();
        timestampLabel.setText("label text");

        p = new JPanel();
        p.add(timestampLabel);
        container.add(p);


        JLabel l = new JLabel("dummy");
        infoPanel = new JPanel();
        infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.PAGE_AXIS));
        infoPanel.add(l);
        container.add(infoPanel);


        f.setSize(500, 300);
        f.show();
    }


    public void replaceLabelText(String s){
        this.timestampLabel.setText(s);
    }



    public void clearInfoFields(){
        for(Component component: this.infoPanel.getComponents()){
            this.infoPanel.remove(component);
        }
    }

    public void createRow(String content, int mode, ActionListener actionListener){
        JPanel jp = new JPanel();
        jp.add(new JLabel(content));

        if(mode == 0){
            jp.add(createBlankButton());
        }
        else if (mode == 1){
            jp.add(createButtonToBeTaken(actionListener));
        }
        else{
            jp.add(createButtonNotTaken());
        }

        this.infoPanel.add(jp);
    }

    private JButton createBlankButton(){
        return new JButton("Not yet");
    }

    private JButton createButtonToBeTaken(ActionListener actionListener){
        JButton jb = new JButton("Take now!");
        jb.addActionListener(actionListener);
        jb.setBackground(Color.GREEN);
        return jb;
    }

    private JButton createButtonNotTaken(){
        JButton jb = new JButton("Time passed, not taken");
        jb.setBackground(Color.RED);
        return jb;
    }

}
