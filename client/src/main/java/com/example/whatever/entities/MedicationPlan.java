package com.example.whatever.entities;


public class MedicationPlan implements java.io.Serializable{

    private Integer id;
    private Integer patient;
    private String medication;
    private Integer interval;
    private Integer period;

    public MedicationPlan() {
    }

    public MedicationPlan(Integer id, Integer patient, String medication, Integer interval, Integer period) {
        super();
        this.id = id;
        this.patient = patient;
        this.medication = medication;
        this.interval = interval;
        this.period = period;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPatient() {
        return this.patient;
    }

    public void setPatient(Integer patient) {
        this.patient = patient;
    }

    public String getMedication() {
        return this.medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    // in minutes
    public Integer getInterval() {
        return this.interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    // in days
    public Integer getPeriod() {
        return this.period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    @Override
    public String toString(){
        return id + "; " + patient + "; " + medication + "; " + period + "; " + interval + '\n';
    }
}
