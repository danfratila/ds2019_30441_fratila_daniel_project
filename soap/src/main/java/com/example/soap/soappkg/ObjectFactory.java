
package com.example.soap.soappkg;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.example.soap.soappkg package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ViewAbnormalActivities_QNAME = new QName("http://soap.whatever.example.com/", "viewAbnormalActivities");
    private final static QName _ViewActivityNamesResponse_QNAME = new QName("http://soap.whatever.example.com/", "viewActivityNamesResponse");
    private final static QName _ViewDataForActivityResponse_QNAME = new QName("http://soap.whatever.example.com/", "viewDataForActivityResponse");
    private final static QName _ViewAbnormalActivitiesResponse_QNAME = new QName("http://soap.whatever.example.com/", "viewAbnormalActivitiesResponse");
    private final static QName _AnnotatePatientBehaviorResponse_QNAME = new QName("http://soap.whatever.example.com/", "annotatePatientBehaviorResponse");
    private final static QName _ViewTakenPlanAtDayResponse_QNAME = new QName("http://soap.whatever.example.com/", "viewTakenPlanAtDayResponse");
    private final static QName _ViewActivityHistoryResponse_QNAME = new QName("http://soap.whatever.example.com/", "viewActivityHistoryResponse");
    private final static QName _AddRecommendationResponse_QNAME = new QName("http://soap.whatever.example.com/", "addRecommendationResponse");
    private final static QName _AddRecommendation_QNAME = new QName("http://soap.whatever.example.com/", "addRecommendation");
    private final static QName _ViewTakenPlanAtDay_QNAME = new QName("http://soap.whatever.example.com/", "viewTakenPlanAtDay");
    private final static QName _ViewActivityHistory_QNAME = new QName("http://soap.whatever.example.com/", "viewActivityHistory");
    private final static QName _ViewDataForActivity_QNAME = new QName("http://soap.whatever.example.com/", "viewDataForActivity");
    private final static QName _AnnotatePatientBehavior_QNAME = new QName("http://soap.whatever.example.com/", "annotatePatientBehavior");
    private final static QName _ViewActivityNames_QNAME = new QName("http://soap.whatever.example.com/", "viewActivityNames");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.example.soap.soappkg
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ViewActivityHistoryResponse }
     * 
     */
    public ViewActivityHistoryResponse createViewActivityHistoryResponse() {
        return new ViewActivityHistoryResponse();
    }

    /**
     * Create an instance of {@link AddRecommendationResponse }
     * 
     */
    public AddRecommendationResponse createAddRecommendationResponse() {
        return new AddRecommendationResponse();
    }

    /**
     * Create an instance of {@link AnnotatePatientBehaviorResponse }
     * 
     */
    public AnnotatePatientBehaviorResponse createAnnotatePatientBehaviorResponse() {
        return new AnnotatePatientBehaviorResponse();
    }

    /**
     * Create an instance of {@link ViewTakenPlanAtDayResponse }
     * 
     */
    public ViewTakenPlanAtDayResponse createViewTakenPlanAtDayResponse() {
        return new ViewTakenPlanAtDayResponse();
    }

    /**
     * Create an instance of {@link ViewAbnormalActivitiesResponse }
     * 
     */
    public ViewAbnormalActivitiesResponse createViewAbnormalActivitiesResponse() {
        return new ViewAbnormalActivitiesResponse();
    }

    /**
     * Create an instance of {@link ViewDataForActivityResponse }
     * 
     */
    public ViewDataForActivityResponse createViewDataForActivityResponse() {
        return new ViewDataForActivityResponse();
    }

    /**
     * Create an instance of {@link ViewActivityNamesResponse }
     * 
     */
    public ViewActivityNamesResponse createViewActivityNamesResponse() {
        return new ViewActivityNamesResponse();
    }

    /**
     * Create an instance of {@link ViewAbnormalActivities }
     * 
     */
    public ViewAbnormalActivities createViewAbnormalActivities() {
        return new ViewAbnormalActivities();
    }

    /**
     * Create an instance of {@link ViewTakenPlanAtDay }
     * 
     */
    public ViewTakenPlanAtDay createViewTakenPlanAtDay() {
        return new ViewTakenPlanAtDay();
    }

    /**
     * Create an instance of {@link AnnotatePatientBehavior }
     * 
     */
    public AnnotatePatientBehavior createAnnotatePatientBehavior() {
        return new AnnotatePatientBehavior();
    }

    /**
     * Create an instance of {@link ViewActivityNames }
     * 
     */
    public ViewActivityNames createViewActivityNames() {
        return new ViewActivityNames();
    }

    /**
     * Create an instance of {@link ViewActivityHistory }
     * 
     */
    public ViewActivityHistory createViewActivityHistory() {
        return new ViewActivityHistory();
    }

    /**
     * Create an instance of {@link ViewDataForActivity }
     * 
     */
    public ViewDataForActivity createViewDataForActivity() {
        return new ViewDataForActivity();
    }

    /**
     * Create an instance of {@link AddRecommendation }
     * 
     */
    public AddRecommendation createAddRecommendation() {
        return new AddRecommendation();
    }

    /**
     * Create an instance of {@link AnnotationDTO }
     * 
     */
    public AnnotationDTO createAnnotationDTO() {
        return new AnnotationDTO();
    }

    /**
     * Create an instance of {@link PatientDataDTO }
     * 
     */
    public PatientDataDTO createPatientDataDTO() {
        return new PatientDataDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewAbnormalActivities }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.whatever.example.com/", name = "viewAbnormalActivities")
    public JAXBElement<ViewAbnormalActivities> createViewAbnormalActivities(ViewAbnormalActivities value) {
        return new JAXBElement<ViewAbnormalActivities>(_ViewAbnormalActivities_QNAME, ViewAbnormalActivities.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewActivityNamesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.whatever.example.com/", name = "viewActivityNamesResponse")
    public JAXBElement<ViewActivityNamesResponse> createViewActivityNamesResponse(ViewActivityNamesResponse value) {
        return new JAXBElement<ViewActivityNamesResponse>(_ViewActivityNamesResponse_QNAME, ViewActivityNamesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewDataForActivityResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.whatever.example.com/", name = "viewDataForActivityResponse")
    public JAXBElement<ViewDataForActivityResponse> createViewDataForActivityResponse(ViewDataForActivityResponse value) {
        return new JAXBElement<ViewDataForActivityResponse>(_ViewDataForActivityResponse_QNAME, ViewDataForActivityResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewAbnormalActivitiesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.whatever.example.com/", name = "viewAbnormalActivitiesResponse")
    public JAXBElement<ViewAbnormalActivitiesResponse> createViewAbnormalActivitiesResponse(ViewAbnormalActivitiesResponse value) {
        return new JAXBElement<ViewAbnormalActivitiesResponse>(_ViewAbnormalActivitiesResponse_QNAME, ViewAbnormalActivitiesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnnotatePatientBehaviorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.whatever.example.com/", name = "annotatePatientBehaviorResponse")
    public JAXBElement<AnnotatePatientBehaviorResponse> createAnnotatePatientBehaviorResponse(AnnotatePatientBehaviorResponse value) {
        return new JAXBElement<AnnotatePatientBehaviorResponse>(_AnnotatePatientBehaviorResponse_QNAME, AnnotatePatientBehaviorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewTakenPlanAtDayResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.whatever.example.com/", name = "viewTakenPlanAtDayResponse")
    public JAXBElement<ViewTakenPlanAtDayResponse> createViewTakenPlanAtDayResponse(ViewTakenPlanAtDayResponse value) {
        return new JAXBElement<ViewTakenPlanAtDayResponse>(_ViewTakenPlanAtDayResponse_QNAME, ViewTakenPlanAtDayResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewActivityHistoryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.whatever.example.com/", name = "viewActivityHistoryResponse")
    public JAXBElement<ViewActivityHistoryResponse> createViewActivityHistoryResponse(ViewActivityHistoryResponse value) {
        return new JAXBElement<ViewActivityHistoryResponse>(_ViewActivityHistoryResponse_QNAME, ViewActivityHistoryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddRecommendationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.whatever.example.com/", name = "addRecommendationResponse")
    public JAXBElement<AddRecommendationResponse> createAddRecommendationResponse(AddRecommendationResponse value) {
        return new JAXBElement<AddRecommendationResponse>(_AddRecommendationResponse_QNAME, AddRecommendationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddRecommendation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.whatever.example.com/", name = "addRecommendation")
    public JAXBElement<AddRecommendation> createAddRecommendation(AddRecommendation value) {
        return new JAXBElement<AddRecommendation>(_AddRecommendation_QNAME, AddRecommendation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewTakenPlanAtDay }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.whatever.example.com/", name = "viewTakenPlanAtDay")
    public JAXBElement<ViewTakenPlanAtDay> createViewTakenPlanAtDay(ViewTakenPlanAtDay value) {
        return new JAXBElement<ViewTakenPlanAtDay>(_ViewTakenPlanAtDay_QNAME, ViewTakenPlanAtDay.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewActivityHistory }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.whatever.example.com/", name = "viewActivityHistory")
    public JAXBElement<ViewActivityHistory> createViewActivityHistory(ViewActivityHistory value) {
        return new JAXBElement<ViewActivityHistory>(_ViewActivityHistory_QNAME, ViewActivityHistory.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewDataForActivity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.whatever.example.com/", name = "viewDataForActivity")
    public JAXBElement<ViewDataForActivity> createViewDataForActivity(ViewDataForActivity value) {
        return new JAXBElement<ViewDataForActivity>(_ViewDataForActivity_QNAME, ViewDataForActivity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnnotatePatientBehavior }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.whatever.example.com/", name = "annotatePatientBehavior")
    public JAXBElement<AnnotatePatientBehavior> createAnnotatePatientBehavior(AnnotatePatientBehavior value) {
        return new JAXBElement<AnnotatePatientBehavior>(_AnnotatePatientBehavior_QNAME, AnnotatePatientBehavior.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewActivityNames }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.whatever.example.com/", name = "viewActivityNames")
    public JAXBElement<ViewActivityNames> createViewActivityNames(ViewActivityNames value) {
        return new JAXBElement<ViewActivityNames>(_ViewActivityNames_QNAME, ViewActivityNames.class, null, value);
    }

}
