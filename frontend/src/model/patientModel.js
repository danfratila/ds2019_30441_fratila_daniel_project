import { EventEmitter } from "events";
import RestPatient from "../rest/RestPatient";

        
class PatientModel extends EventEmitter {
    constructor() {
        super();
        this.restPatient = new RestPatient();
        this.state = {
            patients: [],
            newPatient: {
                firstname: "",
                lastname: "",
                birthdate: "",
                gender: "",
                address: "",
                record: ""
            },
            updatedPatient: {
                patientId: "",
                firstname: "",
                lastname: "",
                birthdate: "",
                gender: "",
                address: "",
                record: ""
            },
            filterTag : "",
            filteredPatients: [],
            deleteId: "",
            specificPatients: [],
            notifications: [],
            activities: ['January', 'February', 'March', 'April', 'May', 'June', 'Q'],
            data: [65, 59, 80, 81, 56, 55, 50],
            abnormalActivities: [],
            annotatedActivity:{
                activityId: "",
                annotation: ""
            },
            recommendationActivity:{
                activityId: "",
                recommendation: ""
            },

        };
    }

    loadAbnormalActivities(){
        return this.restPatient.loadAbnormalActivities().then(activities =>{
            this.state = {
                ...this.state,
                abnormalActivities: activities
            };
            this.emit("change", this.state);
        })
    }

    loadNotifications(){
        return this.restPatient.loadDataForPatient1().then(notifications => {
            this.state = {
                ...this.state,
                notifications: notifications
            };
            this.emit("change", this.state);
        })
    }

    loadActivitiesCharts(){
        return this.restPatient.loadActivitiesForPatient1Charts().then(activities => {
            this.state = {
                ...this.state,
                activities: activities
            };
            this.emit("change", this.state);
        })
    }

    loadDataCharts(){
        return this.restPatient.loadDataForPatient1Charts().then(data => {
            this.state = {
                ...this.state,
                data: data
            };
            this.emit("change", this.state);
        })
    }

    appendNotifications(notification){
        this.state = {
            ...this.state,
            notifications: this.state.notifications.concat([notification])
        };
        this.emit("change", this.state);
    }




    loadPatients() {
        return this.restPatient.loadAllPatients().then(patients => {
            this.state = { 
                ...this.state,
                patients: patients
            };
            this.emit("change", this.state);
        })
    }


    // --------------------------------------------------------
    // add
    addPatient(firstName, lastName, birthdate, gender, address, record){
        return this.restPatient.createPatient(firstName, lastName, birthdate, gender, address, record)
            .then(patient => this.appendPatient(patient));
    }

    appendPatient(patient){
        this.state = {
            ...this.state,
            patients: this.state.patients.concat([patient])
        };
        this.emit("change", this.state);
    }

    changeNewPatientProperty(property, value) {
        this.state = {
            ...this.state,
            newPatient: {
                ...this.state.newPatient,
                [property]: value
            }
        };
        this.emit("change", this.state);
    }


    // --------------------------------------------------------
    // filter
    filterByName(){
        return this.restPatient.loadCertainPatientByName(this.state.filterTag)
            .then(patients => {
                this.state = {
                    ...this.state,
                    filteredPatients: patients
                };
                this.emit("change", this.state);
            });
    }

    changeFilterProperty(value){
        this.state = {
            ...this.state,
            filterTag: value
        };
        this.emit("change", this.state);
    }

    clearPreviousFilteredPatients(){
        this.state = {
            ...this.state,
            filteredPatients: []
        };
        this.emit("change", this.state);
    }

    clearFilters(){
        this.state = {
            ...this.state,
            filterTag: "",
            filteredPatients: []
        };
        this.emit("change", this.state);
    }

    // --------------------------------------------------------
    // update
    changeUpdatedPatientProperty(property, value) {
        this.state = {
            ...this.state,
            updatedPatient: {
                ...this.state.updatedPatient,
                [property]: value
            }
        };
        this.emit("change", this.state);
    }

    updatePatient(id, firstName, lastName, birthdate, gender, address, record){
        return this.restPatient.updatePatient(id, firstName, lastName, birthdate, gender, address, record);
    }


    // --------------------------------------------------------
    // delete
    changeDeletePatientProperty(value){
        this.state = {
            ...this.state,
            deleteId: value
        };
        this.emit("change", this.state);
    }

    deletePatient(id){
        return this.restPatient.deletePatientById(id);
    }


    appendSpecificPatient(patient){
        this.state = {
            ...this.state,
            specificPatients: this.state.specificPatients.concat([patient])
        };
        this.emit("change", this.state);
    }

    loadCertainPatients(username){
        return this.restPatient.loadCertainPatients(username)
            .then(patients =>{
                this.state.specificPatients = [];
                patients.forEach(p => this.appendSpecificPatient(p));

                this.emit("change", this.state);
            });
    }


    changeAnnotationProperty(property, value){
        this.state = {
            ...this.state,
            annotatedActivity: {
                ...this.state.annotatedActivity,
                [property]: value
            }
        };
        this.emit("change", this.state);
    }

    addAnnotation(id, text){
        return this.restPatient.createAnnotation(id, text);
    }

    changeRecommendationProperty(property, value){
        this.state = {
            ...this.state,
            recommendationActivity: {
                ...this.state.recommendationActivity,
                [property]: value
            }
        };
        this.emit("change", this.state);
    }

    addRecommendation(id, text){
        return this.restPatient.createRecommendation(id, text);
    }

}


const patientModel = new PatientModel();


export default patientModel;
