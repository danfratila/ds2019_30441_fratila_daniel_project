import { EventEmitter } from "events";
import RestUser from "../rest/RestUser";
import WebSocketListener from "../ws/WebSocketListener";
import patientModel from "./patientModel";

class UserModel extends EventEmitter {
    listener = new WebSocketListener("", "");

    constructor() {
        super();
        this.restUser = new RestUser();
        this.state = {
            loggedUser:{
                username: "",
                password: "",
                role: ""
            }
        };
    }

    logUser(){
        return this.restUser.getUser(this.state.loggedUser.username, this.state.loggedUser.password)
            .then(user =>{
                this.state = {
                    ...this.state,
                    loggedUser: {
                        ...this.state.loggedUser,
                        role: user.role
                    }
                };
                const listener = new WebSocketListener(this.state.loggedUser.username, this.state.loggedUser.password);
                console.log("ajung dupa create web socket");
                listener.on("event", event => {
                    console.log("ajung in listener");
                    if (event.type === "NOTIFICATION_CREATED") {
                    patientModel.appendNotifications(event.notification);
                    console.log("apenduit");
                    }
                });
                console.log("ajung dupa listener");
                this.emit("change", this.state);
            });
    }



    changeLoggedUserProperty(property, value) {
        this.state = {
            ...this.state,
            loggedUser: {
                ...this.state.loggedUser,
                [property]: value
            }
        };
        this.emit("change", this.state);
    }

    validateLoggedUser() {
        return !(this.state.loggedUser.role === "");
    }
}


const userModel = new UserModel();

userModel.listener.on("event", event =>{

});

export default userModel;
