import { EventEmitter } from "events";
import RestMedication from "../rest/RestMedication";


class MedicationModel extends EventEmitter {
    constructor() {
        super();
        this.restMedication = new RestMedication();
        this.state = {
            medications: [],
            newMedication: {
                name: "",
                sideEffects: "",
                dosage: ""
            },
            updatedMedication: {
                name: "",
                sideEffects: "",
                dosage: ""
            },
            filterTag: "",
            filteredMedications: [],
            deleteName: ""
        };
    }

    loadMedications() {
        return this.restMedication.loadAllMedications().then(medications => {
            this.state = { 
                ...this.state,
                medications: medications
            };
            this.emit("change", this.state);
        })
    }


    // --------------------------------------------------------
    // add
    addMedication(name, sideEffects, dosage){
        return this.restMedication.createMedication(name, sideEffects, dosage)
            .then(medication => this.appendMedication(medication))
    }

    appendMedication(medication){
        this.state = {
            ...this.state,
            medications: this.state.medications.concat([medication])
        };
        this.emit("change", this.state);
    }

    changeNewMedicationProperty(property, value) {
        this.state = {
            ...this.state,
            newMedication: {
                ...this.state.newMedication,
                [property]: value
            }
        };
        this.emit("change", this.state);
    }


    // --------------------------------------------------------
    // filter
    filterByName(){
        return this.restMedication.loadCertainMedicationByName(this.state.filterTag).then(medications => {
            this.state = {
                ...this.state,
                filteredMedications: medications
            };
            this.emit("change", this.state);
        });
    }

    changeFilterProperty(value){
        this.state = {
            ...this.state,
            filterTag: value
        };
        this.emit("change", this.state);
    }

    clearPreviousFilteredMedications(){
        this.state = {
            ...this.state,
            filteredMedications: []
        };
        this.emit("change", this.state);
    }

    clearFilters(){
        this.state = {
            ...this.state,
            filterTag: "",
            filteredMedications: []
        };
        this.emit("change", this.state);
    }


    // --------------------------------------------------------
    // update
    changeUpdatedMedicationProperty(property, value) {
        this.state = {
            ...this.state,
            updatedMedication: {
                ...this.state.updatedMedication,
                [property]: value
            }
        };
        this.emit("change", this.state);
    }

    updateMedication(name, sideEffects, dosage){
        return this.restMedication.updateMedication(name, sideEffects, dosage).then(medications => {
            console.log(medications);
        });
    }


    // --------------------------------------------------------
    // delete
    changeDeleteMedicationProperty(value){
        this.state = {
            ...this.state,
            deleteName: value
        };
        this.emit("change", this.state);
    }

    deleteMedication(name){
        return this.restMedication.deleteMedication(name);
    }
}

const medicationModel = new MedicationModel();

export default medicationModel;
