import React, { Component } from "react";
import userModel from "../../model/userModel";

import LogUser from "./LogUser";
import logUserPresenter from "../../presenter/userPresenter/logUserPresenter";

const mapModelStateToComponentState = modelState => ({
    username: modelState.loggedUser.username,
    password: modelState.loggedUser.password,
    role: modelState.loggedUser.role
});


export default class SmartUsers extends Component {
    constructor() {
        super();
        this.state = mapModelStateToComponentState(userModel.state);
        this.listener = modelState => this.setState(mapModelStateToComponentState(modelState));
        userModel.addListener("change", this.listener);
    }

    componentWillUnmount() {
        userModel.removeListener("change", this.listener);
    }

    render() {
        return (
            <div>
                {/*Validate user every time a page is loaded*/}
                {logUserPresenter.onValidateLoggedUser()}

                <LogUser
                    username={this.state.username}
                    password={this.state.password}
                    onChange={logUserPresenter.onChange}
                    onLogIn={logUserPresenter.onLogIn}
                />
            </div>
        );
    }
}
