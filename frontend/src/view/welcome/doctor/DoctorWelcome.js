import React from "react";

const DoctorWelcome = ({ onGoToPatients, onGoToCaregivers, onGoToMedications, onGoToCharts}) => (
    <div>
        <h2>View or modify:</h2>
        <div>
            <button onClick={onGoToPatients}>patients</button>
            <br/>
            <button onClick={onGoToCaregivers}>caregivers</button>
            <br/>
            <button onClick={onGoToMedications}>medications</button>
            <br/>
            <button onClick={onGoToCharts}>view charts</button>
        </div>
    </div>
);

export default DoctorWelcome;
