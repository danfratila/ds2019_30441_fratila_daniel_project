import React from "react";
import {Bar} from "react-chartjs-2";


const ActivitiesChart = ({ activities, title, data}) => (
    <div>
        <Bar
            data={{
                labels: activities,
                datasets: [
                    {
                        label: title,
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132,1)',
                        borderWidth: 1,
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        hoverBorderColor: 'rgba(255,99,132,1)',
                        data: data
                    }
                ]
            }}
            width={100}
            height={500}
            options={{maintainAspectRatio: false}}/>
    </div>
);

export default ActivitiesChart;
