import React from "react";

const AbnormalActivitiesList = ({ activities,
                                    activityIdAnnotation, annotationText, onAnnotate, onChangeAnnotation,
                                    activityIdRecommendation, recommendationText, onRecommend, onChangeRecommendation}) => (
    <div>
        <h2>{ "Paranormal Activities" }</h2>
        <table border="1">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Start time</th>
                <th>End time</th>
            </tr>
            </thead>
            <tbody>
            {
                activities.map((activity, index) => (
                    <tr key={index} >
                        <td>{activity.id}</td>
                        <td>{activity.activity}</td>
                        <td>{Date(activity.startTime)}</td>
                        <td>{Date(activity.endTime)}</td>
                    </tr>
                ))
            }
            </tbody>
        </table>



        <h2>{ "Create an annotation" }</h2>
        <label>Activity id:</label>
        <input value={activityIdAnnotation} onChange={ e => onChangeAnnotation("activityId", e.target.value) } />
        <br/>
        <label>Normal or not?</label>
        <input value={annotationText} onChange={ e => onChangeAnnotation("annotation", e.target.value) } />
        <br/>
        <button onClick={onAnnotate} >Annotate!</button>
        <br/>



        <h2>{ "Recommend something" }</h2>
        <label>Activity id:</label>
        <input value={activityIdRecommendation} onChange={ e => onChangeRecommendation("activityId", e.target.value) } />
        <br/>
        <label>Suggest something</label>
        <input value={recommendationText} onChange={ e => onChangeRecommendation("recommendation", e.target.value) } />
        <br/>
        <button onClick={onRecommend} >Recommend!</button>
    </div>
);

export default AbnormalActivitiesList;
