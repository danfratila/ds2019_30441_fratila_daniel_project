import React from "react";

const MedicationsList = ({ medications, title}) => (
    <div>
        <h2>{ title || "Medications" }</h2>
        <table border="1">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Side effects</th>
                <th>Dosage (per day)</th>
            </tr>
            </thead>
            <tbody>
            {
                medications.map((medication, index) => (
                    <tr key={index} >
                        <td>{medication.id}</td>
                        <td>{medication.name}</td>
                        <td>{medication.sideEffects}</td>
                        <td>{medication.dosage}</td>
                    </tr>
                ))
            }
            </tbody>
        </table>
    </div>
);

export default MedicationsList;
