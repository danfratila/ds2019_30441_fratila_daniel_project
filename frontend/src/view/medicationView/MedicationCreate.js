import React from "react";

const MedicationCreate = ({ newName, newSideEffects, newDosage, onCreate, onChange }) => (
    <div>
        <h2>Create or update medication</h2>
        <div>
            <label>Medication name: </label>
            <input value={newName}
                onChange={ e => onChange("name", e.target.value) } />
            <br />
            <label>Side effects: </label>
            <input value={newSideEffects}
                onChange={ e => onChange("sideEffects", e.target.value) } />
            <br />
            <label>Dosage per day: </label>
            <input value={newDosage}
                onChange={ e => onChange("dosage", e.target.value) } />
            <br />

            <button onClick={onCreate} >Create!</button>
        </div>
    </div>
);

export default MedicationCreate;
