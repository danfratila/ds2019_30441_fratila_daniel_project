import React from "react";

const MedicationDelete = ({ deleteName, onChangeDeleteName, onDelete}) => (
    <div>
        <h2>{"Delete medications"}</h2>
        <label>Delete medications with following name:</label>
        &emsp;
        <input value={deleteName}
               onChange={e => onChangeDeleteName(e.target.value)}/>
        <br/>
        <button className="btn btn-primary" onClick={onDelete}>Delete!</button>

    </div>
);

export default MedicationDelete;
