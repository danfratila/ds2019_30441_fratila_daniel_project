import caregiverModel from "../../model/caregiverModel";


class CreateCaregiverPresenter {
    onCreate() {
        caregiverModel.addCaregiver(
            caregiverModel.state.newCaregiver.firstname,
            caregiverModel.state.newCaregiver.lastname,
            caregiverModel.state.newCaregiver.birthdate,
            caregiverModel.state.newCaregiver.gender,
            caregiverModel.state.newCaregiver.address
        ).then(() => {
                caregiverModel.changeNewCaregiverProperty("firstname", "");
                caregiverModel.changeNewCaregiverProperty("lastname", "");
                caregiverModel.changeNewCaregiverProperty("birthdate", "");
                caregiverModel.changeNewCaregiverProperty("gender", "");
                caregiverModel.changeNewCaregiverProperty("address", "");
                caregiverModel.loadCaregivers();
        });
    }

    onChange(property, value) {
        caregiverModel.changeNewCaregiverProperty(property, value);
    }
}

const createCaregiverPresenter = new CreateCaregiverPresenter();

export default createCaregiverPresenter;
