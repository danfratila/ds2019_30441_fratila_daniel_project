import caregiverModel from "../../model/caregiverModel";

class UpdateCaregiverPresenter {
    onUpdateCaregiver(){
        caregiverModel.updateCaregiver(
            caregiverModel.state.updatedCaregiver.caregiverId,
            caregiverModel.state.updatedCaregiver.firstname,
            caregiverModel.state.updatedCaregiver.lastname,
            caregiverModel.state.updatedCaregiver.birthdate,
            caregiverModel.state.updatedCaregiver.gender,
            caregiverModel.state.updatedCaregiver.address
        ).then( () => {
                caregiverModel.changeUpdatedCaregiverProperty("caregiverId", "") ;
                caregiverModel.changeUpdatedCaregiverProperty("firstname", "") ;
                caregiverModel.changeUpdatedCaregiverProperty("lastname", "") ;
                caregiverModel.changeUpdatedCaregiverProperty("birthdate", "") ;
                caregiverModel.changeUpdatedCaregiverProperty("gender", "") ;
                caregiverModel.changeUpdatedCaregiverProperty("address", "") ;
                caregiverModel.loadCaregivers();
            });
    }

    onChange(property, value) {
        caregiverModel.changeUpdatedCaregiverProperty(property, value);
    }
}

const updateCaregiverPresenter = new UpdateCaregiverPresenter();

export default updateCaregiverPresenter;
