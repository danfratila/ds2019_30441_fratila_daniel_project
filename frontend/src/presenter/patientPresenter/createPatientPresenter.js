import patientModel from "../../model/patientModel";


class CreatePatientPresenter {
    onCreate() {
        patientModel.addPatient(
            patientModel.state.newPatient.firstname,
            patientModel.state.newPatient.lastname,
            patientModel.state.newPatient.birthdate,
            patientModel.state.newPatient.gender,
            patientModel.state.newPatient.address,
            patientModel.state.newPatient.record
        ).then(() => {
                patientModel.changeNewPatientProperty("firstname", "");
                patientModel.changeNewPatientProperty("lastname", "");
                patientModel.changeNewPatientProperty("birthdate", "");
                patientModel.changeNewPatientProperty("gender", "");
                patientModel.changeNewPatientProperty("address", "");
                patientModel.changeNewPatientProperty("record", "");
                patientModel.loadPatients();
        });
    }

    onChange(property, value) {
        patientModel.changeNewPatientProperty(property, value);
    }
}

const createPatientPresenter = new CreatePatientPresenter();

export default createPatientPresenter;
