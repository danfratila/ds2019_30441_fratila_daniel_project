import patientModel from "../../model/patientModel";
import userModel from "../../model/userModel";


class ListPatientsPresenter {
    onInit() {
        patientModel.loadPatients();
    }

    onLoadCertainPatients(){
        patientModel.loadCertainPatients(userModel.state.loggedUser.username).then(() =>
            console.log("Specific:", patientModel.state.specificPatients));
    }
}

const listPatientsPresenter = new ListPatientsPresenter();

export default listPatientsPresenter;
