
class WelcomeDoctorPresenter {

    onGoToPatients(){
        window.location.assign("#/doctor/patients");
    }

    onGoToCaregivers(){
        window.location.assign("#/doctor/caregivers");
    }

    onGoToMedications(){
        window.location.assign("#/doctor/medications");
    }

    onGoToCharts(){
        window.location.assign("#/doctor/activities");
    }
}

const welcomeDoctorPresenter = new WelcomeDoctorPresenter();

export default welcomeDoctorPresenter;
