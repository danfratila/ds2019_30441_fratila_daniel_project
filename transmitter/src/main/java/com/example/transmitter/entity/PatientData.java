package com.example.transmitter.entity;

import javax.persistence.*;


@Entity
@Table(name = "infos")
public class PatientData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String patient_id;
    private String activity;
    private Long startTime;
    private Long endTime;


    public String getPatient_id(){
        return patient_id;
    }

    public void setPatient_id(String patient_id){
        this.patient_id = patient_id;
    }

    public String getActivity(){
        return activity;
    }

    public void setActivity(String activity){
        this.activity = activity;
    }

    public Long getStartTime(){
        return startTime;
    }

    public void setStartTime(Long startTime){
        this.startTime = startTime;
    }

    public Long getEndTime(){
        return endTime;
    }

    public void setEndTime(Long endTime){
        this.endTime = endTime;
    }


    @Override
    public String toString()
    {
        return "{\n \"patient_id\" : \"" + this.getPatient_id() +
                "\"\n \"activity\" : \"" + this.getActivity() +
                "\"\n \"start\" : \"" + this.getStartTime().toString() +
                "\"\n \"end\" : \"" + this.getEndTime().toString() +
                "\"\n}\n";
    }

}
